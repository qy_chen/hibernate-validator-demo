# hibernate-validator

[SSM项目示例DEMO](https://gitee.com/qy_chen/hibernate-validator-demo)

# API

### **JSR提供的校验注解**

```xml-dtd
@Null   被注释的元素必须为 null     **(验证对象是否为null)    
@NotNull    被注释的元素必须不为 null,无法检查长度为0的字串  **(用在基本类型上,包括null )   
@AssertTrue     被注释的元素必须为 true    
@AssertFalse    被注释的元素必须为 false    
@Min(value)     被注释的元素必须是一个数字，其值必须大于等于指定的最小值    
@Max(value)     被注释的元素必须是一个数字，其值必须小于等于指定的最大值    
@DecimalMin(value)  被注释的元素必须是一个数字，其值必须大于等于指定的最小值    
@DecimalMax(value)  被注释的元素必须是一个数字，其值必须小于等于指定的最大值    
@Size(max=, min=)   被注释的元素的大小必须在指定的范围内    
@Digits (integer, fraction)     被注释的元素必须是一个数字，其值必须在可接受的范围内    
@Past   被注释的元素必须是一个过去的日期    
@Future     被注释的元素必须是一个将来的日期    
@Pattern(regex=,flag=)  被注释的元素必须符合指定的正则表达式
```

### **Hibernate Validator提供的校验注解**

```xml-dtd
@NotBlank(message =)   验证字符串非null，且长度必须大于0  **(用String上面,包括""和null)  
@Email  被注释的元素必须是电子邮箱地址    
@Length(min=,max=)  被注释的字符串的大小必须在指定的范围内    
@NotEmpty   被注释的字符串的必须非空  **(用在集合上面)
@Range(min=,max=,message=)  被注释的元素必须在合适的范围内
```

## API分类

### 空检查

```xml-dtd
@Null   被注释的元素必须为 null     **(验证对象是否为null)    
@NotNull    被注释的元素必须不为 null,无法检查长度为0的字串  **(用在基本类型上,包括null )  
@NotBlank(message =)   验证字符串非null，且长度必须大于0  **(用String上面,包括""和null)  
@NotEmpty   被注释的字符串的必须非空  **(用在集合上面)
```

### Boolean检查

```xml-dtd
@AssertTrue     被注释的元素必须为 true    
@AssertFalse    被注释的元素必须为 false 
```

### 长度检查

```xml-dtd
@Size(max=, min=)   被注释的元素的大小必须在指定的范围内  
@Length(min=,max=)  被注释的字符串的大小必须在指定的范围内  
```

### 日期检查

```xml-dtd
@Past   被注释的元素必须是一个过去的日期    
@Future     被注释的元素必须是一个将来的日期  
```

### 数值检查

​	**建议使用在String,Integer类型,不建议使用在int类型上,因为表单值为""时无法转换为int,但可以转换为String为"",Integer为null;**

```xml-dtd
@Min(value)     被注释的元素必须是一个数字，其值必须大于等于指定的最小值    
@Max(value)     被注释的元素必须是一个数字，其值必须小于等于指定的最大值    
@DecimalMin(value)  被注释的元素必须是一个数字，其值必须大于等于指定的最小值    
@DecimalMax(value)  被注释的元素必须是一个数字，其值必须小于等于指定的最大值   
@Digits (integer, fraction)     被注释的元素必须是一个数字，其值必须在可接受的范围内    
@Range(min=,max=,message=)  被注释的元素必须在合适的范围内
```

### 其它

```xml-dtd
@Valid 递归的对关联对象进行校验,如果关联对象是个集合或者数组,那么对其中的元素进行递归校验,如果是一个map,则对其中的值部分进行校验.(是否进行递归验证)
@Email  被注释的元素必须是电子邮箱地址    
@ScriptAssert(lang= , script= , alias=) 类级约束,评估一个脚本表达式对注释的元素
@URL(protocol= ,host= , port= ,regexp= , flags= ) 验证URL地址是否有效
```

