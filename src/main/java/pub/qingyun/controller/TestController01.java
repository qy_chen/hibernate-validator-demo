package pub.qingyun.controller;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import pub.qingyun.bean.SysUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pub.qingyun.bean.UserChecks;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author CQY
 * @Date 2020/2/15/015 11:56
 * @Version 1.0
 **/
@Controller
public class TestController01 {


    /**
     * @RequestBody 接收的是一个Json对象的字符串，而不是一个Json对象。然而在ajax请求往往传的都是Json对象，
     * 后来发现用 JSON.stringify(data)的方式就能将对象变成字符串。
     * 同时ajax请求的时候也要指定dataType: "json",contentType:"application/json"
     * 这样就可以轻易的将一个对象或者List传到Java端，使用@RequestBody即可绑定对象或者List.
     * @ModelAttribute :用于接收key，value形式参数
     * <p>
     * 此方法会先从model去获取key为"user"的对象,如果获取不到会通过反射实例化一个User对象,再从request里面拿值set到这个对象,
     * 然后把这个User对象添加到model(其中key为"user").
     */
    @GetMapping("/test.do")
    @ResponseBody
    public Object test001(@Valid @ModelAttribute SysUser user, BindingResult result) {
        if (result.hasErrors()) {
            //如果没有通过,跳转提示
            Map<String, String> map = getErrors(result);
            return map;
        } else {
            //继续业务逻辑
        }
        return "ok";
    }

    /**
     * @Validated(value = { UserChecks.class}) SysUser user, BindingResult result
     * 含义为：
     * 对SysUser这个形参进行校验，选择他其中定义的UserChecks这个分组的校验规则。
     * 注意：如果你需要进行校验，在方法参数中，需要在校验对象的前后加上@Validated和BindingResult bindingResult，他们是成对出现的。
     */

    @GetMapping("/test02.do")
    @ResponseBody
    public Object test002(@Validated(value = {UserChecks.class}) SysUser user, BindingResult result) {
        if (result.hasErrors()) {
            //如果没有通过,跳转提示
            Map<String, String> map = getErrors(result);
            return map;
        } else {
            //继续业务逻辑
        }
        return "ok";
    }


    private Map<String, String> getErrors(BindingResult result) {
        Map<String, String> map = new HashMap<String, String>();
        List<FieldError> list = result.getFieldErrors();
        for (FieldError error : list) {
            System.out.println("error.getField():" + error.getField());
            System.out.println("error.getDefaultMessage():" + error.getDefaultMessage());

            map.put(error.getField(), error.getDefaultMessage());
        }
        return map;
    }
}
