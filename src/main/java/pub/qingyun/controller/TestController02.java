package pub.qingyun.controller;


import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pub.qingyun.bean.AjaxResult;
import pub.qingyun.bean.Msg;
import pub.qingyun.exception.BusinessException;

import javax.validation.constraints.Min;

/**
 * @Author CQY
 * @Date 2020/2/15/015 11:56
 * @Version 1.0
 **/
@RestController
@Validated
public class TestController02 {
    /**
     * 用于测试
     *
     * @param id id数不能小于10 @RequestParam类型的参数需要在Controller上增加@Validated
     * @return
     */
    @GetMapping(value = "/info")
    public AjaxResult test(@Min(value = 10, message = "id最小只能是10") @RequestParam("id") Integer id) {
        if (id < 10) {
            throw new BusinessException(("id最小只能是10"));
        }
        System.out.println("id:" + id);
        return AjaxResult.success(id);
    }


}
