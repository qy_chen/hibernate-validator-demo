package pub.qingyun.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pub.qingyun.bean.Msg;
import pub.qingyun.bean.SysUser;
import pub.qingyun.dao.SysUserMapper;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * @Author CQY
 * @Date 2020/2/17/017 17:17
 * @Version 1.0
 **/
@Controller
public class TestController03 {

    @Autowired
    SysUserMapper userMapper;

    @GetMapping(value = "/user", produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public Msg selectUserById(@RequestParam(name = "id") BigDecimal id) {
        Optional.ofNullable(userMapper.selectByPrimaryKey(id)).map(SysUser::toString).get();

        SysUser user = Optional.ofNullable(userMapper.selectByPrimaryKey(id)).orElse(null);
        return Msg.success().add("user", user);

        //return Optional.ofNullable(userMapper.selectByPrimaryKey(id)).map(SysUser::toString).orElse("未查询到用户:" + id);


    }
}
