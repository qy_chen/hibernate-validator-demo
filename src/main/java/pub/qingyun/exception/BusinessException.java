package pub.qingyun.exception;

/**
 * 业务异常
 *
 * @Author CQY
 * @Date 2020/2/16/016 16:17
 * @Version 1.0
 **/
public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    protected final String message;

    public BusinessException(String message) {
        this.message = message;
    }

    public BusinessException(String message, Throwable e) {
        super(message, e);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
