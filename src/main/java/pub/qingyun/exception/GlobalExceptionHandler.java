package pub.qingyun.exception;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pub.qingyun.bean.Msg;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author CQY
 * @Date 2020/2/16/016 15:14
 * @Version 1.0
 **/

/**
 * 统一异常处理类
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Msg handleException(HttpServletRequest request, HttpServletResponse response, Object o, Exception e) {
        System.out.println("GlobalExceptionHandler.handleException...");
        return Msg.fail().add("errorMsg", StringUtils.isEmpty(e.getMessage()) ? "未知异常" : e.getMessage());
    }
}
