package pub.qingyun.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @Author CQY
 * @Date 2020/2/16/016 16:57
 * @Version 1.0
 **/
public class ContextPathListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String contextPath = sce.getServletContext().getContextPath();
        //获取web.xml中的context-param参数
        System.out.println("contextConfigLocation: " + sce.getServletContext().getInitParameter("contextConfigLocation"));
        sce.getServletContext().setAttribute("APP_PATH", contextPath);
        sce.getServletContext().setInitParameter("test","AAAABBBB");

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("ContextPathListener.contextDestroyed...");
    }
}
