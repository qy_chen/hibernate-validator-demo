package pub.qingyun;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pub.qingyun.bean.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author CQY
 * @Date 2020/2/19/019 14:18
 * @Version 1.0
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext-mybatis.xml"})
public class JdbcTemplateTestDemo {

    private static final Logger logger = LoggerFactory.getLogger(JdbcTemplateTestDemo.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 创建表
     */
    @Test
    public void create() {
        String sql = "create table tb_test2020021901 (id integer,user_name varchar2(40),password varchar2(40))";
        jdbcTemplate.execute(sql);
    }

    /**
     * 第一个参数为执行sql
     * 第二个参数为参数数据
     * 第三个参数为参数类型
     */
    @Test
    public void save1() {
        User user = new User();
        user.setUsername("张三");
        user.setPassword("123456");
        String sql = "insert into tb_test2020021901 (user_name,password) values(?,?)";
        jdbcTemplate.update(sql,
                new Object[]{user.getUsername(), user.getPassword()},
                new int[]{java.sql.Types.VARCHAR, java.sql.Types.VARCHAR}
        );
    }

    //避免sql注入
    @Test
    public void save2() {
        User user = new User();
        user.setUsername("李四");
        user.setPassword("123456");
        jdbcTemplate.update("insert into tb_test2020021901(user_name,password) values(?,?)",
                new PreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps) throws SQLException {
                        ps.setString(1, user.getUsername());
                        ps.setString(2, user.getPassword());
                    }
                });
    }

    //jdbcTemplate.update适合于insert 、update和delete操作；

    /**
     * 第一个参数为执行sql
     * 第二个参数为参数数据
     */
    @Test
    public void save3() {
        User user = new User();
        user.setUsername("王五");
        user.setPassword("123456");
        jdbcTemplate.update("insert into tb_test2020021901(user_name,password) values(?,?)",
                new Object[]{user.getUsername(), user.getPassword()});
    }

    //返回插入的主键
    @Test
    public void save4() {
        User user = new User();
        user.setId(1006);
        user.setUsername("赵六");
        user.setPassword("123456");
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
                                @Override
                                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                                    PreparedStatement ps = connection.prepareStatement(
                                            "insert into tb_test2020021901 values (?,?,?)", new String[]{"id"});
                                    ps.setInt(1, user.getId());
                                    ps.setString(2, user.getUsername());
                                    ps.setString(3, user.getPassword());
                                    return ps;
                                }
                            },
                keyHolder);
        System.out.println(keyHolder.getKeyList());
    }

    @Test
    public void delete() {
        int index = jdbcTemplate.update(
                "delete from tb_test2020021901 where id = ?",
                new Object[]{1006},
                new int[]{Types.INTEGER});
        System.out.println("----------------------" + index);
    }


    @Test
    public void queryForObject1() {
        Integer index = jdbcTemplate.queryForObject("select count(0) from tb_test2020021901", Integer.class);
        System.out.println("----------------------" + index);
    }

    @Test
    public void queryForObject2() {
        Integer index = jdbcTemplate.queryForObject(
                "select count(0) from tb_test2020021901 where user_name = ?",
                Integer.class, "张三");
        System.out.println("----------------------" + index);
    }

    @Test
    public void queryForObject3() {
        String result = jdbcTemplate.queryForObject(
                "select user_name from tb_test2020021901 where id = ? "
                , new Object[]{1001}, String.class);
        System.out.println("----------------------" + result);
    }

    @Test
    public void queryForObject4() {
        User user = jdbcTemplate.queryForObject(
                "select * from tb_test2020021901 where id = ?",
                new Object[]{1001},
                new BeanPropertyRowMapper<>(User.class));
        System.out.println("----------------------" + user);
    }

    @Test
    public void queryForObject5() {
        List<User> users = jdbcTemplate.query(
                "select * from tb_test2020021901 where id = ?"
                , new BeanPropertyRowMapper<>(User.class), 1001);
        System.out.println("----------------------" + users);
    }

    /**
     * 通过RowCallbackHandler对Select语句得到的每行记录进行解析
     * ，并为其创建一个User数据对象。实现了手动的OR映射。
     */
    @Test
    public void queryUserById4() {
        User user = new User();
        //该方法返回值为void
        this.jdbcTemplate.query("select * from tb_test2020021901 where id = ?",
                new Object[]{1001},
                new RowCallbackHandler() {
                    @Override
                    public void processRow(ResultSet rs) throws SQLException {
                        user.setId(rs.getInt("id"));
                        user.setUsername(rs.getString("user_name"));
                        user.setPassword(rs.getString("password"));
                    }
                });
        System.out.println(user);
    }

    //批量操作适合于增、删、改操作
    @Test
    public void batchUpdate() {
        List<User> users = new ArrayList();
        User user1 = new User(1001, "张三01", "654321");
        User user2 = new User(1002, "李四02", "654321");
        User user3 = new User(1003, "王五03", "654321");
        users.add(user1);
        users.add(user2);
        users.add(user3);
        int[] updateCounts = jdbcTemplate.batchUpdate(
                "update tb_test2020021901 set user_name = ?, password = ? where id = ?",
                new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setString(1, ((User) users.get(i)).getUsername());
                        ps.setString(2, ((User) users.get(i)).getPassword());
                        ps.setLong(3, ((User) users.get(i)).getId());
                    }

                    @Override
                    public int getBatchSize() {
                        System.out.println("users.size():" + users.size());
                        return users.size();
                    }
                }
        );
        System.out.println(Arrays.toString(updateCounts));

    }

    //调用存储过程
    @Test
    public void callProcedure() {
        int index = jdbcTemplate.update("call SUPPORT.REFRESH_USERS_SUMMARY(?)", new Object[]{Long.valueOf(1001)});
    }
}