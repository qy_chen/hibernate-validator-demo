package pub.qingyun;

import pub.qingyun.bean.SysUser;
import org.junit.Test;
import pub.qingyun.bean.UserChecks;
import pub.qingyun.utils.ValidationUtil;

import javax.validation.*;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @Author CQY
 * @Date 2020/2/15/015 11:42
 * @Version 1.0
 **/
public class JunitTestDemo {

    @Test
    public void testHelloworld() {
        //创建校验工程(默认)
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        //获取校验对象
        Validator validator = factory.getValidator();
        SysUser user = new SysUser();
        user.setUserId(new BigDecimal(101));
        user.setDeptId(new BigDecimal(99));
        user.setUserName("test001");
        //使用校验的对象,校验对象模型
        Set<ConstraintViolation<SysUser>> constraintViolations = validator.validate(user);
        //校验bean的某一个属性
        //Set<ConstraintViolation<SysUser>> validateProperty = validator.validateProperty(user, "loginName");
        //只校验对象中的某个属性,并通过参数对该属性赋值进行校验
        //Set<ConstraintViolation<SysUser>> validateValue = validator.validateValue(SysUser.class, "userName", "test");

        System.out.println("校验失败条目:" + constraintViolations.size());
        Iterator<ConstraintViolation<SysUser>> iterator = constraintViolations.iterator();
        while (iterator.hasNext()) {
            ConstraintViolation<SysUser> cv = iterator.next();
            //校验出错的字段
            System.out.println("校验出错的字段: " + cv.getPropertyPath());
            System.out.println("错误信息: " + cv.getMessage());
        }


    }
    @Test
    public void testHelloworldGroup() {
        //创建校验工程(默认)
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        //获取校验对象
        Validator validator = factory.getValidator();
        SysUser user = new SysUser();
        user.setUserId(new BigDecimal(101));
        user.setDeptId(new BigDecimal(99));
        //user.setUserName("test001");
        //使用校验的对象,校验对象模型
        Set<ConstraintViolation<SysUser>> constraintViolations = validator.validate(user, UserChecks.class);

        System.out.println("校验失败条目:" + constraintViolations.size());
        Iterator<ConstraintViolation<SysUser>> iterator = constraintViolations.iterator();
        while (iterator.hasNext()) {
            ConstraintViolation<SysUser> cv = iterator.next();
            //校验出错的字段
            System.out.println("校验出错的字段: " + cv.getPropertyPath());
            System.out.println("错误信息: " + cv.getMessage());
        }


    }

    @Test
    public void testValidator() {
        SysUser user = new SysUser();
        user.setUserId(new BigDecimal(101));
        user.setDeptId(new BigDecimal(99));
        user.setUserName("test001");
        user.setRemark("2019-12-03 13:02:56");
        ValidationUtil.ValidResult validResult = ValidationUtil.validateBean(user);
        if (validResult.hasErrors()) {
            String errors = validResult.getErrors();
            System.out.println(errors);
        }
    }

    @Test
    public void testValidator02() {
        SysUser user = new SysUser();
        user.setUserId(new BigDecimal(101));
        user.setDeptId(new BigDecimal(99));
        user.setUserName("test001");
        ValidationUtil.ValidResult validResult = ValidationUtil.validateProperty(user, "loginName");
        if (validResult.hasErrors()) {
            String errors = validResult.getErrors();
            System.out.println(errors);
        }
    }


    @Test
    public void testLombok() {
        SysUser user = new SysUser();
        user.setUserId(new BigDecimal(101));
        user.setLoginName("test001");
        user.setUserName("测试1");
        System.out.println(user);
    }
    @Test
    public void testOptional() {
        SysUser user = new SysUser();
        user.setUserId(new BigDecimal(101));
        user.setLoginName("test001");
        user.setUserName("测试1");
        Map<String,SysUser> userMap = new HashMap<>();
        userMap.put("user", user);

    }
}
