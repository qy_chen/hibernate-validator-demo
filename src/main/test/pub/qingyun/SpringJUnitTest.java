package pub.qingyun;

import pub.qingyun.bean.SysUser;
import pub.qingyun.dao.SysUserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.sql.*;

/**
 * @author cqy
 * @Date 2019/10/26 17:30
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext-mybatis.xml"})
public class SpringJUnitTest {
    private static final Logger logger = LoggerFactory.getLogger(SpringJUnitTest.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Autowired
    private SqlSessionFactoryBean sqlSessionFactoryBean;

    @Autowired
    SysUserMapper userMapper;

    @Test
    public void testUserMapper() {
        SysUser user = userMapper.selectByPrimaryKey(new BigDecimal(1));
        System.out.println("-------------------------");
        System.out.println(user);
    }


    @Test
    public void testJdbc() {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String url = "jdbc:oracle:thin:@192.168.31.100:1521:xe";
            String user = "system";
            String password = "oracle";
            Connection conn = DriverManager.getConnection(url, user, password);
            String sql = "SELECT * FROM Sys_User";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String name = rs.getString("LOGIN_NAME");
                System.out.println("name:" + name);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
