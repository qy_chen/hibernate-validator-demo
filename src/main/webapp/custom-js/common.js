(function ($) {
    //原型方法封装
    //$.extend($.fn, {
    $.fn.extend({
        style: function (bgcolor, wigth, height) {
            this.css({'backgroundColor': bgcolor, 'width': wigth, 'height': height});
        }
    });
    //静态方法封装
    $.extend({
        operate: {
            add: function (num1, num2) {
                return num1 + num2;
            },
            mins: function (num1, num2) {
                return num1 - num2;
            }
        }
    });
}(jQuery));